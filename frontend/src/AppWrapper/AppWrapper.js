import React from 'react';
import { HashRouter as Router} from "react-router-dom";
import { AlbumsProvider, PlaylistProvider, ArtistsProvider, MoviesProvider, AudioProvider, PlaylistsProvider, SettingsProvider, MusicIsPlayingProvider } from '../Context/Context';
import Navigation from '../Navigation/Navigation';
import PageWrapper from '../Pages/PageWrapper';
import MusicPlayer from '../Components/MusicPlayer/MusicPlayer';
import './AppWrapper.scss';

const ProviderWrapper = (props) => {
    return (
        <AlbumsProvider>
            <ArtistsProvider>
                <MoviesProvider>
                    <AudioProvider>
                        <PlaylistProvider>
                            <PlaylistsProvider>
                                <SettingsProvider>
                                    <MusicIsPlayingProvider>
                                        {props.children}
                                    </MusicIsPlayingProvider>
                                </SettingsProvider>
                            </PlaylistsProvider>
                        </PlaylistProvider>
                    </AudioProvider>
                </MoviesProvider>
            </ArtistsProvider>
        </AlbumsProvider>
    )
}

const AppWrapper = () => {
    
    return (
        <Router>
            <div className="application__outer-wrapper">
                <ProviderWrapper>
                    <div className="content__outer-wrapper">
                        <Navigation/>
                        <PageWrapper/>
                    </div>
                    <MusicPlayer/>
                </ProviderWrapper>
            </div>
        </Router>
    );
}

export default AppWrapper;