import React from 'react';
import './MusicPlayer.scss';
import IconShuffle from '../../Resources/Icons/IconShuffle';
import IconPlay from '../../Resources/Icons/IconPlay';
import IconPause from '../../Resources/Icons/IconPause';
import IconRepeat from '../../Resources/Icons/IconRepeat';
import IconNext from '../../Resources/Icons/IconNext';
import IconPrevious from '../../Resources/Icons/IconPrevious';
import { AudioContext, PlaylistContext, MusicIsPlayingContext } from '../../Context/Context';
import IconVolume01 from '../../Resources/Icons/IconVolume01';
import IconVolume02 from '../../Resources/Icons/IconVolume02';
import IconVolume03 from '../../Resources/Icons/IconVolume03';
import IconVolume04 from '../../Resources/Icons/IconVolume04';

//TODO add fallback image / artist / track name 

const MusicPlayerInfo = ({audioData}) => {
    console.log(Object.keys(audioData).length);
    
    return (
        <div className="music-player--info__wrapper">
            {Object.keys(audioData).length &&
                <>
                    <div className="music-player--image"><img alt="album--cover" src={audioData.albumImageSource}></img></div>
                    <div className="music-player--info">
                        <div className="music-player--title">{audioData.trackName}</div>
                        <div className="music-player--artist">{audioData.trackArtistsReadable}{audioData.trackFeatArtistsReadable && `, ${audioData.trackFeatArtistsReadable}`}</div>
                    </div>
                </>
            }
        </div>
    )
}

const MusicPlayerControls = ({queuePreviousTrack, queueNextTrack, playerSeek, playerPlay, playerPause, audioData, audioIsPlaying, setLoopAudio, loopAudio, seeker, audioProgress}) => {
    const musicIsPlayingContext = React.useContext(MusicIsPlayingContext);
    
    const handlePlayNext = () => {
        //check if audio data is loaded
        if(audioData.trackName) {
            //next check if there is a next track
            if(audioData.trackNextTrackSource) {
                queueNextTrack();
            } else {
                console.log('there is no next track to be played, disable next button');
            }
        } else {
            console.log ('no track loaded, disable next button');
        }
    }

    const handlePlayPrevious = () => {
        //check if audio data is loaded
        if(audioData.trackName) {
            //previous check if there is a previous track
            if(audioData.trackPreviousTrackSource) {
                queuePreviousTrack();
            } else {
                console.log('there is no previous track to be played, disable previous button');
            }
        } else {
            console.log ('no track loaded, disable previous button');
        }
    }

    const handlePause = () => {
        audioData.trackName && playerPause();
        musicIsPlayingContext.contextSetMusicIsPlaying(false);
    }

    const handlePlay = () => {
        audioData.trackName && playerPlay();
        musicIsPlayingContext.contextSetMusicIsPlaying(true);
    }

    return (
        <div className="music-player--controls__wrapper">
            <div className="music-player--control__top">            
                {/* <div className="shuffle__wrapper">
                    <button type="button" onClick={() => {}} className="media--button button--shuffle"><IconShuffle/></button>
                </div> */}
                <div className="previous__wrapper">
                    <button type="button" onClick={(e) => {handlePlayPrevious(e)}} className="media--button button--previous"><IconPrevious/></button>
                </div>
                <div className="play__wrapper">
                    {audioIsPlaying ? 
                        <button type="button" onClick={() => {handlePause()}} className="media--button button--pause"><IconPause/></button>  
                        : 
                        <button type="button" onClick={() => {handlePlay()}} className="media--button button--play"><IconPlay/></button>
                    }
                </div>
                <div className="next__wrapper">
                    <button type="button"  onClick={(e) => {handlePlayNext(e)}} className="media--button button--next"><IconNext/></button>
                </div>
                {/* <div className={`repeat__wrapper ${loopAudio ? `enabled` : `disabled`}`}>
                    <button type="button" onClick={() => {setLoopAudio(!loopAudio)}} className="media--button button--repeat"><IconRepeat/></button>
                </div> */}
            </div>
            <div className="music-player--control__bottom">
                <div className="seek__wrapper">
                    <div ref={seeker} className="seek--outer" onClick={(e) => {playerSeek(e)}}>
                        <div 
                            className="seek--inner" 
                            style={{width: `${audioProgress * 100}%`}} 
                            >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const MusicPlayerVolume = ({playerVolume, setPlayerVolume}) => {

    const renderVolumeIcon = () => {
        if(playerVolume === 0) {
            return <IconVolume01/>
        } else if (playerVolume < 0.33) {
            return <IconVolume02/>
        } else if (playerVolume < 0.66) {
            return <IconVolume03/>
        } else {
            return <IconVolume04/>
        }
    }

    return (
        <div className="music-player--volume__wrapper">          
            {renderVolumeIcon()}
            <div className="slidecontainer">
                <input onChange={(e) => {setPlayerVolume(e.target.value / 100)}} type="range" min="1" max="100" value={playerVolume * 100} className="slider" id="myRange"/>
            </div>
        </div>
    )
}

const MusicPlayer = () => {
    //TODO send a track from the DB as a property to the player each time we want to play a track. Wrap the player in a player manager with access to the currentlyPlaying context and send obj as property to MusicPlayer

    const audioContext = React.useContext(AudioContext);
    const playlistContext = React.useContext(PlaylistContext);
    const musicIsPlayingContext = React.useContext(MusicIsPlayingContext);

    //audio tag reference
    const player = React.useRef();
    const seeker = React.useRef();

    //audio volume hook
    const [playerVolume, setPlayerVolume] = React.useState(1);

    //audio tag hooks
    const [loopAudio, setLoopAudio] = React.useState(false);

    //control hooks
    const [audioIsPlaying, setAudioIsPlaying] = React.useState(false);
    const [audioProgress, setAudioProgress] = React.useState();

    const queuePreviousTrack = () => {
        let newTrack;
        if(playlistContext.playlistData.albumTrackList) {
            newTrack = playlistContext.playlistData.albumTrackList[audioContext.audioData.trackPreviousId];
        } else {
            newTrack = playlistContext.playlistData.playlistTrackList[audioContext.audioData.trackPreviousId];
        }
        
        audioContext.contextWriteAudio({
            ...newTrack
        })
    }

    React.useEffect(() => {
        player.current.volume = playerVolume;
        if(musicIsPlayingContext.musicIsPlaying) {
            playerPlay();
        } else {
            playerPause();
        }
    });

    const queueNextTrack = () => {
        if(audioContext.audioData.trackNextTrackSource) {
            let newTrack;
            console.log('CLICKED');
            if(playlistContext.playlistData.albumTrackList) {
                newTrack = playlistContext.playlistData.albumTrackList[audioContext.audioData.trackNextTrackId];
            } else {
                newTrack = playlistContext.playlistData.playlistTrackList[audioContext.audioData.trackNextTrackId];
            }
            audioContext.contextWriteAudio({
                ...newTrack
            })
        }         
    }

    const handleTrackEnded = (fn) => {
        //do stuff here when track is finished playing
        console.log("Track is finished playing through")
        setAudioIsPlaying(false);
        queueNextTrack();
    }    

    const handleLoadedData = () => {
        //do stuff here when track is loaded
        console.log("Track is finished loading and is ready for play");
        player.current.play();
        setAudioIsPlaying(true);
    }

    const playerPlay = () => {
        player.current.play();
        setAudioIsPlaying(true);
    }

    const playerPause = () => {
        player.current.pause();
        setAudioIsPlaying(false);
    }

    const playerSeek = (e) => {
        let percent = Math.round(e.nativeEvent.offsetX / seeker.current.offsetWidth * 100) / 100;
        let newPercent = percent * player.current.duration;
        player.current.currentTime = newPercent;
        setAudioProgress(percent);
    }

    const updateTime = () => {
        let progress = player.current.currentTime / player.current.duration;
        setAudioProgress(progress);
    }

    return (
        <div className="music-player__outer-wrapper">
            <MusicPlayerInfo audioData={audioContext.audioData}/>
            <audio 
                src={audioContext.audioData.trackSource} 
                autoPlay={true} 
                loop={loopAudio} 
                onEnded={() => {handleTrackEnded()}}
                onLoadedData={() => {handleLoadedData()}}
                onTimeUpdate={() => {updateTime()}}
                ref={player}
            />
            
            <MusicPlayerControls 
                audioData={audioContext.audioData}
                setLoopAudio={setLoopAudio} 
                audioProgress={audioProgress} 
                seeker={seeker} 
                audioIsPlaying={audioIsPlaying} 
                loopAudio={loopAudio}
                playerPlay={playerPlay} 
                playerPause={playerPause}
                playerSeek={playerSeek}
                queueNextTrack={queueNextTrack}
                queuePreviousTrack={queuePreviousTrack}
            />
            <MusicPlayerVolume playerVolume={playerVolume} setPlayerVolume={setPlayerVolume} />
        </div>
        
    );
}

export default MusicPlayer;