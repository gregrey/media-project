import React from 'react';
import './SearchBar.scss';
import IconSearch from '../../Resources/Icons/IconSearch';

const SearchBar = (props) => {
    const [searchValue, setSearchValue] = React.useState("");

    const handleSearchInput = (e) => {
        setSearchValue(e.target.value);
    }

    const searchSubmit = (e) => {
        e.preventDefault();
        props.setSearchParam(searchValue);
    }

    return (
        <div className="search-bar__outer-wrapper">
            <div className="input--search__wrapper">
                <form onSubmit={(e) => {searchSubmit(e)}} className="search-bar--form__wrapper">
                    <label htmlFor="search"><IconSearch/></label>
                    <input 
                        autoComplete="off" 
                        id="search" 
                        type="text" 
                        value={searchValue} 
                        className="search-bar--input" 
                        placeholder="Search library" 
                        onChange={ (e) => {handleSearchInput(e)} 
                    }/>
                </form>
            </div>
        </div>
    );
}

export default SearchBar;