import React from 'react';

//Init global context variables
const AlbumsContext = React.createContext();
const ArtistsContext = React.createContext();
const MoviesContext = React.createContext();
const AudioContext = React.createContext();
const PlaylistContext = React.createContext();
const PlaylistsContext = React.createContext();
const SettingsContext = React.createContext();
const MusicIsPlayingContext = React.createContext();

const fetchData = (url, writeData) => {
    fetch(url).then(res => res.json()).then(
        result => { writeData(result) },
        error => { console.log(error) }
    );
}

//Init global providers
const AlbumsProvider = (props) => {
    const [albumsData, setAlbumsData] = React.useState({});
    React.useEffect(() => { fetchData("http://localhost:1337/get/albums", setAlbumsData) }, []);
    return (
        <AlbumsContext.Provider value={{ albumsData, contextWriteAlbums: (data) => { setAlbumsData(data) } }}>
            {props.children}
        </AlbumsContext.Provider>
    )
}
const ArtistsProvider = (props) => {
    const [artistsData, setArtistsData] = React.useState({});
    return (
        <ArtistsContext.Provider value={{ artistsData, contextWriteArtists: (data) => { setArtistsData(data) } }}>
            {props.children}
        </ArtistsContext.Provider>
    )
}
const MoviesProvider = (props) => {
    const [moviesData, setMoviesData] = React.useState({});
    React.useEffect(() => { fetchData("http://localhost:1337/get/movies", setMoviesData) }, []);
    return (
        <MoviesContext.Provider value={{ moviesData, contextWriteMovies: (data) => { setMoviesData(data) } }}>
            {props.children}
        </MoviesContext.Provider>
    )
}
const AudioProvider = (props) => {
    const [audioData, setAudioData] = React.useState({});
    return (
        <AudioContext.Provider value={{ audioData, contextWriteAudio: (data) => { setAudioData(data) } }}>
            {props.children}
        </AudioContext.Provider>
    )
}
const PlaylistProvider = (props) => {
    const [playlistData, setPlaylistData] = React.useState({});
    return (
        <PlaylistContext.Provider value={{ playlistData, contextWritePlaylist: (data) => { setPlaylistData(data) } }}>
            {props.children}
        </PlaylistContext.Provider>
    )
}
const PlaylistsProvider = (props) => {
    const [playlistsData, setPlaylistsData] = React.useState({});
    React.useEffect(() => { fetchData("http://localhost:1337/get/playlists", setPlaylistsData) }, []);
    return (   
        <PlaylistsContext.Provider value={{ playlistsData, contextWritePlaylists: (data) => { setPlaylistsData(data) } }}>
            {props.children}
        </PlaylistsContext.Provider>
    )
}
const SettingsProvider = (props) => {
    const [settingsData, setSettingsData] = React.useState({});
    return (
        <SettingsContext.Provider value={{ settingsData, contextWriteSettings: (data) => { setSettingsData(data) } }}>
            {props.children}
        </SettingsContext.Provider>
    )
}

const MusicIsPlayingProvider = (props) => {
    const [musicIsPlaying, setMusicIsPlaying] = React.useState(false);
    return (
        <MusicIsPlayingContext.Provider value={{ musicIsPlaying, contextSetMusicIsPlaying: (bool) => {setMusicIsPlaying(bool)} }}>
            {props.children}
        </MusicIsPlayingContext.Provider>
    )
}

export {
    AlbumsContext,
    AlbumsProvider,
    ArtistsContext,
    ArtistsProvider,
    MoviesContext,
    MoviesProvider,
    MusicIsPlayingContext,
    MusicIsPlayingProvider,
    AudioContext,
    AudioProvider,
    PlaylistContext,
    PlaylistProvider,
    PlaylistsContext,
    PlaylistsProvider,
    SettingsContext,
    SettingsProvider
}