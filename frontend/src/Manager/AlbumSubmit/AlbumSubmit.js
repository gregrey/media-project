import React from 'react';
import './AlbumSubmit.scss';
import { Link } from 'react-router-dom'
import IconArrowLeft from '../../Resources/Icons/IconArrowLeft';
import { AlbumsContext } from '../../Context/Context';

const defaultAlbum = {
    albumArtists: "",
    albumFolder: "",
    albumDiscogs: "",
    albumExtension: "mp3",
    albumGenres: "",
    albumLabel: "",
    albumName: "",
    albumRelease: "",
    albumTrackAmount: "",
    albumTracks: []
}

const defaultTrack = {
    trackName: "",
    trackArtists: "",
    trackFeatArtists: "",
    trackRemixArtists: ""
}

const AlbumSubmit = () => {

    const albumsContext = React.useContext(AlbumsContext);

    //album hooks
    const [albumFolder, setAlbumFolder] = React.useState(defaultAlbum.albumFolder);
    const [albumArtists, setAlbumArtists] = React.useState(defaultAlbum.albumArtists);
    const [albumDiscogs, setAlbumDiscogs] = React.useState(defaultAlbum.albumDiscogs);
    const [albumExtension, setAlbumExtension] = React.useState(defaultAlbum.albumExtension);
    const [albumGenres, setAlbumGenres] = React.useState(defaultAlbum.albumGenres);
    const [albumLabel, setAlbumLabel] = React.useState(defaultAlbum.albumLabel);
    const [albumName, setAlbumName] = React.useState(defaultAlbum.albumName);
    const [albumRelease, setAlbumRelease] = React.useState(defaultAlbum.albumRelease);
    const [albumTrackAmount, setAlbumTrackAmount] = React.useState(defaultAlbum.albumTrackAmount);
    const [albumTracks, setAlbumTracks] = React.useState([{...defaultTrack}]);
    const [albumSubmitted, setAlbumSubmitted] = React.useState(false);
    const [scrapeLink, setScrapeLink] = React.useState("");

    //helper hooks
    const [formError, setFormError] = React.useState(false);

    //add track to album tracks array with default values
    const handleAddTrack = () => {
        setAlbumTracks(previous => [...previous, {
            ...defaultTrack,
            trackArtists: albumArtists
        }]);
    }

    //handle track value change
    const handleTrackChange = (e, i, key) => {
        setAlbumTracks([...albumTracks], albumTracks[i][key] = e.target.value);
    }

    //rest form function
    const resetForm = (resetSuccess) => {
        setAlbumArtists(defaultAlbum.albumArtists);
        setAlbumFolder(defaultAlbum.albumFolder);
        setAlbumDiscogs(defaultAlbum.albumDiscogs);
        setAlbumExtension(defaultAlbum.albumExtension);
        setAlbumGenres(defaultAlbum.albumGenres);
        setAlbumLabel(defaultAlbum.albumLabel);
        setAlbumName(defaultAlbum.albumName);
        setAlbumRelease(defaultAlbum.albumRelease);
        setAlbumTrackAmount(defaultAlbum.albumTrackAmount);
        setAlbumTracks([{...defaultTrack}]);
        setFormError(false);
        setScrapeLink("");
        resetSuccess && setAlbumSubmitted(false);
    }

    const handleScrape = (e) => {
        e.preventDefault();
        fetch(`http://localhost:1337/scrape/album/${scrapeLink}`).then(res => res.json()).then(
            result => { 
                console.log(result);
                setAlbumName(result.albumInfo.albumName);
                setAlbumDiscogs(result.albumInfo.albumDiscogs);
                setAlbumLabel(result.albumInfo.albumLabel);
                setAlbumTrackAmount(result.albumInfo.albumTrackAmount);
                setAlbumArtists(result.albumInfo.albumArtists);
                setAlbumTracks([...result.albumTracks]);
                //console.log(result.albumTracks)
            },
            error => { console.log(error) }
        );
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(albumArtists.length > 0 && albumName.length > 0) {
            //post album details to API 
            fetch('http://localhost:1337/post/album', {
                method: 'POST',
                body: JSON.stringify({
                    albumArtists: albumArtists,
                    albumFolder: albumFolder,
                    albumGenres: albumGenres,
                    albumLabel: albumLabel,
                    albumName: albumName,
                    albumExtension: albumExtension,
                    albumRelease: albumRelease,
                    albumDiscogs: albumDiscogs,
                    albumTrackAmount: albumTrackAmount,
                    albumTracks: [...albumTracks]
                }),
                headers: { 'Content-Type': 'application/json' }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    setFormError(false);
                    resetForm();
                    setAlbumSubmitted(true);
                    fetch("http://localhost:1337/get/albums").then(res => res.json()).then(
                        result => { albumsContext.contextWriteAlbums(result)},
                        error => { console.log(error) }
                    );
                    return response;
                } else {
                    setFormError(true);
                    setAlbumSubmitted(false);
                    return response;
                }
            }).catch(err => err);
        } else {
            setFormError(true);
        }

    }

    return (
        <div className="album--submit__outer-wrapper">
            <div className="album--submit-form__wrapper">
                <h1>Add Album</h1>
                <Link className="link--btn__back" to={`/manager`}><IconArrowLeft/> <span>Back to manager</span></Link>
                <form className="album--scrape" onSubmit={handleScrape}>
                    <div className="group-left">
                        <div className="form-section--title__wrapper">
                            <h3>Discogs Scrape</h3>
                        </div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Discogs URL" value={scrapeLink} onChange={(e) => {setScrapeLink(e.target.value)}}/></div>
                        <div className="form--button__wrapper"><button type="submit">Scrape</button></div>
                    </div>
                </form>
                <form onSubmit={handleSubmit} className="album--submit">
                    <div className="group-left">
                        <div className="form-section--title__wrapper">
                            <h3>Album details</h3>
                        </div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Album folder" value={albumFolder} onChange={(e) => {setAlbumFolder(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Album release" value={albumRelease} onChange={(e) => {setAlbumRelease(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Album name" value={albumName} onChange={(e) => {setAlbumName(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Album artists" value={albumArtists} onChange={(e) => {setAlbumArtists(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Album label" value={albumLabel} onChange={(e) => {setAlbumLabel(e.target.value)}}/></div>
                        {/* <div className="form--input__wrapper"><input type="text" placeholder="Album genres" value={albumGenres} onChange={(e) => {setAlbumGenres(e.target.value)}}/></div> */}
                        <div className="form--input__wrapper"><input type="text" placeholder="Album discogs" value={albumDiscogs} onChange={(e) => {setAlbumDiscogs(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Album extension" value={albumExtension} onChange={(e) => {setAlbumExtension(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Album tracks" value={albumTrackAmount} onChange={(e) => {setAlbumTrackAmount(e.target.value)}}/></div>
                        <div className="form--button__wrapper"><button type="submit">Submit</button></div>
                        {formError && <div className="message message--error"><p className="message--text">Please fill in all fields before submitting the form</p></div>}
                        {albumSubmitted && <div className="message message--success"><p className="message--text">Album successfully submitted!</p></div>}
                        <div className="form--button__wrapper"><button className="button--text" onClick={() => {resetForm(true)}} type="button">Reset form</button></div>
                    </div>
                    <div className="group-right">
                        {/* <div className="form-section--title__wrapper"><h3>Album tracks</h3></div> */}
                        {albumTracks.map((track, i) => 
                            <div key={i} className="form--section__wrapper">
                                <h3>Track {("0" + (i + 1)).slice(-2)}</h3>
                                <div className="form--input__wrapper"><input type="text" placeholder="Track name" value={albumTracks[i]["trackName"]} onChange={(e) => {handleTrackChange(e, i, "trackName")}}/></div>
                                <div className="form--input__wrapper"><input type="text" placeholder="Track artists" value={albumTracks[i]["trackArtists"]} onChange={(e) => {handleTrackChange(e, i, "trackArtists")}}/></div>
                                <div className="form--input__wrapper"><input type="text" placeholder="Track feat artists" value={albumTracks[i]["trackFeatArtists"]} onChange={(e) => {handleTrackChange(e, i, "trackFeatArtists")}}/></div>
                                <div className="form--input__wrapper"><input type="text" placeholder="Track remix artists" value={albumTracks[i]["trackRemixArtists"]} onChange={(e) => {handleTrackChange(e, i, "trackRemixArtists")}}/></div>
                            </div>
                        )}
                        <div className="form--button__wrapper"><button onClick={handleAddTrack} type="button">Add track</button></div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default AlbumSubmit;