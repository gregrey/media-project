import React from 'react';
import { Link } from 'react-router-dom'
import './Manager.scss';

const Manager = () => {
    return (
        <div className="manager__outer-wrapper">
            <Link to={`manager/add-album`}>+ Add Album</Link>
            <Link to={`manager/add-playlist`}>+ Add Playlist</Link>
            <Link to={`manager/add-movie`}>+ Add Movie</Link>
            {/* <Link to={`manager/add-artist`}>Add Artist</Link>
            <Link to={`manager/add-movie`}>Add Movie</Link> */}
        </div>
    );
}

export default Manager;