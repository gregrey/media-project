import React from 'react';
import IconArrowLeft from '../../Resources/Icons/IconArrowLeft';
import { Link } from 'react-router-dom'
import './MovieSubmit.scss';
import { MoviesContext } from '../../Context/Context';

const MovieSubmit = () => {

    const moviesContext = React.useContext(MoviesContext);

    console.log(moviesContext);

    //movie hooks
    const [movieName, setMovieName] = React.useState('');
    const [movieRelease, setMovieRelease] = React.useState('');
    const [movieFolderName, setMovieFolderName] = React.useState('');
    const [movieExtension, setMovieExtension] = React.useState('');
    const [movieSubmitted, setMovieSubmitted] = React.useState(false);

    //helper hooks
    const [formError, setFormError] = React.useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        fetch('http://localhost:1337/post/movies', {
            method: 'POST',
            body: JSON.stringify({
                movieName: movieName,
                movieBanner: `${movieFolderName}_banner.jpg`,
                moviePoster: `${movieFolderName}_poster.jpg`,
                movieRelease: movieRelease,
                movieExtension: movieExtension,
                movieFolderName: movieFolderName
            }),
            headers: { 'Content-Type': 'application/json' }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                resetForm();
                setMovieSubmitted(true);
                fetch("http://localhost:1337/get/movies").then(res => res.json()).then(
                    result => { moviesContext.contextWriteMovies(result)},
                    error => { console.log(error) }
                );
                return response;
            } else {
                setFormError(true);
                setMovieSubmitted(false);
                return response;
            }
        }).catch(err => err);
    }

    //rest form function
    const resetForm = (resetSuccess) => {
        setMovieFolderName('');
        setMovieExtension('');
        setMovieName('');
        setMovieRelease('');
        resetSuccess && setMovieSubmitted(false);
    }

    return (
        <div className="movie--submit__outer-wrapper">
            <div className="movie--submit-form__wrapper">
                <h1>Add Movie</h1>
                <Link className="link--btn__back" to={`/manager`}><IconArrowLeft/> <span>Back to manager</span></Link>
                
                <form onSubmit={handleSubmit} className="movie--submit">
                    <div className="group-left">
                        <div className="form-section--title__wrapper">
                            <h3>Movie details</h3>
                        </div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Movie folder" value={movieFolderName} onChange={(e) => {setMovieFolderName(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Movie extension" value={movieExtension} onChange={(e) => {setMovieExtension(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Movie name" value={movieName} onChange={(e) => {setMovieName(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Movie release year" value={movieRelease} onChange={(e) => {setMovieRelease(e.target.value)}}/></div>
                        {formError && <div className="message message--error"><p className="message--text">Please fill in all fields before submitting the form</p></div>}
                        {movieSubmitted && <div className="message message--success"><p className="message--text">Movie successfully submitted!</p></div>}
                        <div className="form--button__wrapper"><button type="submit">Submit</button></div>
                        <div className="form--button__wrapper"><button className="button--text" onClick={() => {resetForm(true)}} type="button">Reset form</button></div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default MovieSubmit;