import React from 'react';
import IconArrowLeft from '../../Resources/Icons/IconArrowLeft';
import { Link } from 'react-router-dom'
import './PlaylistSubmit.scss';
import { PlaylistsContext } from '../../Context/Context';

const PlaylistSubmit = () => {

    const playlistsContext = React.useContext(PlaylistsContext);

    console.log(playlistsContext);

    //playlist hooks
    const [playlistName, setPlaylistName] = React.useState('');
    const [playlistMachineName, setPlaylistMachineName] = React.useState('');
    const [playlistSubmitted, setPlaylistSubmitted] = React.useState(false);

    //helper hooks
    const [formError, setFormError] = React.useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        fetch('http://localhost:1337/post/playlist', {
            method: 'POST',
            body: JSON.stringify({
                playlistName: playlistName,
                playlistMachineName: playlistMachineName,
            }),
            headers: { 'Content-Type': 'application/json' }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                resetForm();
                setPlaylistSubmitted(true);
                fetch("http://localhost:1337/get/playlists").then(res => res.json()).then(
                    result => { playlistsContext.contextWritePlaylists(result)},
                    error => { console.log(error) }
                );
                return response;
            } else {
                setFormError(true);
                setPlaylistSubmitted(false);
                return response;
            }
        }).catch(err => err);
    }

    //rest form function
    const resetForm = (resetSuccess) => {
        setPlaylistName('');
        setPlaylistMachineName('');
        resetSuccess && setPlaylistSubmitted(false);
    }

    return (
        <div className="playlist--submit__outer-wrapper">
            <div className="playlist--submit-form__wrapper">
                <h1>Add Playlist</h1>
                <Link className="link--btn__back" to={`/manager`}><IconArrowLeft/> <span>Back to manager</span></Link>
                
                <form onSubmit={handleSubmit} className="playlist--submit">
                    <div className="group-left">
                        <div className="form-section--title__wrapper">
                            <h3>Playlist details</h3>
                        </div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Playlist name" value={playlistName} onChange={(e) => {setPlaylistName(e.target.value)}}/></div>
                        <div className="form--input__wrapper"><input type="text" placeholder="Playlist machine name" value={playlistMachineName} onChange={(e) => {setPlaylistMachineName(e.target.value)}}/></div>
                        {formError && <div className="message message--error"><p className="message--text">Please fill in all fields before submitting the form</p></div>}
                        {playlistSubmitted && <div className="message message--success"><p className="message--text">Playlist successfully submitted!</p></div>}
                        <div className="form--button__wrapper"><button type="submit">Submit</button></div>
                        <div className="form--button__wrapper"><button className="button--text" onClick={() => {resetForm(true)}} type="button">Reset form</button></div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default PlaylistSubmit;