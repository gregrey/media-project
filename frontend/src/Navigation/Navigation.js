import React from 'react';
import IconBrand02 from '../Resources/Icons/IconBrand02';
import './Navigation.scss';
import { NavLink } from 'react-router-dom'

const Links = (props) => {
    return (
        <nav id="main-nav">
            {/* <NavLink exact activeClassName="active" to="/"><IconGlobe/><span className="nav-text">Discover</span></NavLink> */}
            <NavLink activeClassName="active" to="/music"><span className="nav-text">Music</span></NavLink>
            <NavLink activeClassName="active" to="/playlists"><span className="nav-text">Playlists</span></NavLink>
            <NavLink exact activeClassName="active" to="/movies"><span className="nav-text">Movies</span></NavLink>
            {/* <NavLink exact activeClassName="active" to="/tvshows"><span className="nav-text">TV Shows</span></NavLink> */}
            <NavLink activeClassName="active" to="/manager"><span className="nav-text">Manager</span></NavLink>
        </nav>
    );
}

const Logo = () => {
    return (
        <div className="logo__wrapper">
            <IconBrand02/>
        </div>
    )
}

const Navigation = () => {
    return (
        <aside className="navigation__outer-wrapper">
            <Logo/>
            <Links/>
        </aside>
    );
}

export default Navigation;