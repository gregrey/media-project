import React from 'react';
import { PlaylistContext, AudioContext, MusicIsPlayingContext, PlaylistsContext } from '../../Context/Context';
import './AlbumDetail.scss';
import IconPlay from '../../Resources/Icons/IconPlay';
import IconArrowLeft from '../../Resources/Icons/IconArrowLeft';

const AddToPlaylist = ({track, setShowAddToPlaylist}) => {
    const playlistsContext = React.useContext(PlaylistsContext);
    //add {track} to playlist
    const addToPlaylist = (playlist) => {
        console.log('add track to playlist, ', playlist)
        setShowAddToPlaylist(false);
        fetch('http://localhost:1337/post/edit/playlist/add', {
            method: 'POST',
            body: JSON.stringify({
                playlistMachineName: playlist,
                playlistTrack: track,
            }),
            headers: { 'Content-Type': 'application/json' }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                fetch("http://localhost:1337/get/playlists").then(res => res.json()).then(
                    result => { playlistsContext.contextWritePlaylists(result)},
                    error => { console.log(error) }
                );
                return response;
            } else {
                return response;
            }
        }).catch(err => err);
    }

    const escFunction = React.useCallback((event) => {
        if(event.keyCode === 27) {
        setShowAddToPlaylist(false);
    }}, [setShowAddToPlaylist]);
    
    React.useEffect(() => {
        document.addEventListener("keydown", escFunction, false);
        return () => {
          document.removeEventListener("keydown", escFunction, false);
        };
    }, [escFunction]);

    return(
        <div className="track--add-to-playlist__outer-wrapper">
            <h3>Add track "{track.trackName}" to playlist: </h3>
            <div className="playlist--select__wrapper">
                {Object.keys(playlistsContext.playlistsData).sort().map((playlist, i) => (
                    <React.Fragment key={i}>
                        <div className="playlist-select--item">
                            <button onClick={(e) => {addToPlaylist(playlist)}} type="button">{playlistsContext.playlistsData[playlist].playlistName}</button>
                        </div>
                    </React.Fragment>
                ))}
            </div>
            <div className="form--button__wrapper">
                <button onClick={() => {setShowAddToPlaylist(false)}} className="button--text">cancel</button>
            </div>
        </div>
    )
}

const AlbumTrack = ({album, track, contextWriteAudio, audioData}) => {

    const playlistContext = React.useContext(PlaylistContext);
    const musicIsPlayingContext = React.useContext(MusicIsPlayingContext);
    
    const [trackIsPlaying, setTrackIsPlaying] = React.useState(false);
    const [showAddToPlaylist, setShowAddToPlaylist] = React.useState(false);

    const handlePlayButton = () => {
        console.log(album);
        contextWriteAudio({...track});
        playlistContext.contextWritePlaylist({...album});
        musicIsPlayingContext.contextSetMusicIsPlaying(true);
    }

    const handleAddToPlaylist = () => {
        setShowAddToPlaylist(true);
    }

    React.useEffect(() => {
        //add active state to track if it is in player
        if(track.trackFileName && audioData.trackFileName) {
            setTrackIsPlaying(track.trackFileName.toString() === audioData.trackFileName.toString());
        }
    }, [track.trackFileName, audioData.trackFileName]);
    
    return (
        <div className={`album--track__wrapper ${trackIsPlaying ? `playing` : `disabled`}`}>
            <div className="track--number">
                <button onClick={() => {handlePlayButton()}} className="play-button__wrapper">
                    <IconPlay/>
                </button>
                <span>{track.trackNumber}</span> 
            </div>
            <div className="track--title">
                <span className="title">{track.trackName}</span>
                <span className="artists">&nbsp;-&nbsp;{track.trackArtistsReadable}{track.trackFeatArtistsReadable.length > 0 && `, ${track.trackFeatArtistsReadable}`}</span>
            </div>
            <div className="form--button__wrapper">
                <button className="button--text" onClick={() => {handleAddToPlaylist()}} type="button">Add to playlist</button>
                {showAddToPlaylist && <AddToPlaylist track={track} setShowAddToPlaylist={setShowAddToPlaylist} />}
            </div>
        </div>
    )
}

const AlbumTracks = ({album}) => {
    const audioContext = React.useContext(AudioContext);
    return (
        <>
            {album && 
                <div className="album--tracks__wrapper">
                    <div className="album--track__wrapper">
                        <div className="track--number">{` #`}</div>
                        <div className="track--title"><span className="title">Track name</span></div>
                    </div>
                    {
                        album.albumTrackList.map((track, i) => {
                            return (
                                <AlbumTrack contextWriteAudio={audioContext.contextWriteAudio} audioData={audioContext.audioData} album={album} track={track} key={i}/>
                            )
                        })
                    }
                </div>
            }
        </>
    )
}

const AlbumHeader = ({album, setShowAlbumModal}) => {
    const playlistContext = React.useContext(PlaylistContext);
    const audioContext = React.useContext(AudioContext);
    const musicIsPlayingContext = React.useContext(MusicIsPlayingContext);

    const handlePlayAlbum = () => {
        const track = album.albumTrackList[0];
        audioContext.contextWriteAudio({...track});
        playlistContext.contextWritePlaylist({...album});
        musicIsPlayingContext.contextSetMusicIsPlaying(true);
    }

    return (
        <div className="album--header__wrapper">
            <button className="button--btn__back" onClick={(e) => {setShowAlbumModal('')}}><IconArrowLeft/> <span>Back to album overview</span></button>
            <div className="album--image__wrapper">
                <img alt="album--cover" src={album.albumImageSource} />
            </div>
            <div style={{backgroundImage: `url(${album.albumImageSource})`}} className="album--image--opacity__wrapper">
                {/* <img src={album.albumImageSource} /> */}
            </div>
            <div className="album--text__wrapper">
                <h3>Album</h3>
                <div className="album--title__wrapper">
                    <h1>{album.albumName}</h1>
                </div>
                <div className="album--by__wrapper">
                    <p className="by">By</p>&nbsp;<p className="artist">{album.albumArtistsReadable}</p>
                </div>
                <div className="album--label__wrapper">
                    <p className="release">{album.albumLabel}, {album.albumRelease}</p>
                </div>
                <div className="album--play__wrapper">
                    <div className="form--button__wrapper">
                        <button onClick={() => {handlePlayAlbum()}} type="button">Play album</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

const AlbumDetail = (props) => {
    //const albumName = props.match.params.name;
    const albumName = props.albumName;
    const [album, setAlbum] = React.useState(false);
    
    React.useEffect(() => { 
        fetch(`http://localhost:1337/get/album/${albumName}`).then(res => res.json()).then(
            result => { 
                setAlbum({...result[albumName]});
            },
            error => { console.log(error) }
        );
    }, [albumName]);

    return (
        <div className="album--detail__outer-wrapper">
            {album && 
                <>
                    <AlbumHeader setShowAlbumModal={props.setShowAlbumModal} album={album}/>
                    <AlbumTracks album={album}/>
                </>
            }
        </div>
    );
}

export default AlbumDetail;