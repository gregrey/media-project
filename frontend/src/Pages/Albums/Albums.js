import React from 'react';
import { AlbumsContext, PlaylistContext, AudioContext, MusicIsPlayingContext } from '../../Context/Context';
import { Link } from 'react-router-dom'
import IconPlay from '../../Resources/Icons/IconPlay';
import './Albums.scss';
import SearchBar from '../../Components/Search/SearchBar';
import AlbumDetail from '../AlbumDetail/AlbumDetail';
import { Scrollbars } from 'react-custom-scrollbars';

//show loader until albumsData is loaded from context

const Album = ({album, showAlbumModal, setShowAlbumModal}) => {

    const playlistContext = React.useContext(PlaylistContext);
    const audioContext = React.useContext(AudioContext);
    const musicIsPlayingContext = React.useContext(MusicIsPlayingContext);

    const handlePlayButton = () => {
        const albumFolder = album.albumFolder;
        fetch(`http://localhost:1337/get/album/${albumFolder}`).then(res => res.json()).then(
            result => { 
                const track = result[albumFolder].albumTrackList[0];
                const album = result[albumFolder];
                audioContext.contextWriteAudio({...track});
                playlistContext.contextWritePlaylist({...album});
                musicIsPlayingContext.contextSetMusicIsPlaying(true);
            },
            error => { console.log(error) }
        );
    }

    const openAlbumModal = (e, albumFolder) => {
        e.preventDefault();
        setShowAlbumModal(albumFolder);
    }

    return (
        <div  className="albums--item__wrapper">
            <div className="square">
                <div className="album--play__wrapper">
                    <button onClick={() => {handlePlayButton()}} className="play-button__wrapper">
                        <IconPlay/>
                    </button>
                </div>
            </div>
            <div className="album--cover__wrapper">
                <img alt="album--cover" src={album.albumImageSource} />
            </div>
            
            <div className="album--title__wrapper">
                {/* <Link to={`/music/album/${album.albumFolder}`}>{album.albumName}</Link> */}
                <button type="button" onClick={(e) => {openAlbumModal(e, album.albumFolder)}}>{album.albumName}</button>
            </div>
            <div className="album--artists__wrapper">
                {album.albumArtistsReadable}
            </div>
        </div>
    )
}

const Albums = () => {
    const albumsContext = React.useContext(AlbumsContext);
    const [searchParam, setSearchParam] = React.useState("");

    const [showAlbumModal, setShowAlbumModal] = React.useState('');

    const escFunction = React.useCallback((event) => {
        if(event.keyCode === 27) {
            setShowAlbumModal('');
    }}, [setShowAlbumModal]);
    
    React.useEffect(() => {
        document.addEventListener("keydown", escFunction, false);
        return () => {
          document.removeEventListener("keydown", escFunction, false);
        };
    }, [escFunction]);

    return (
        <div className="albums__outer-wrapper">
            {showAlbumModal.length > 0 && 
                <div className="album-modal__wrapper"><Scrollbars  style={{ }}><AlbumDetail setShowAlbumModal={setShowAlbumModal} albumName={showAlbumModal}/></Scrollbars></div> 
            }
            <div className="albums--list__wrapper">
                <div className="albums--header__wrapper">
                    <h1>Albums</h1>
                    <SearchBar setSearchParam={setSearchParam}/>
                </div>
                {searchParam.length > 0 && 
                    <div className="albums--search-case">
                        <span>Search results for "{searchParam}"</span> 
                        <span className="form--button__wrapper">
                            <button className="button--text" onClick={() => {setSearchParam("")}}>Clear search</button>
                        </span> 
                    </div>
                }
                {Object.keys(albumsContext.albumsData).sort().map((album, i) => (
                    <React.Fragment key={i}>
                        {
                            (
                                albumsContext.albumsData[album].albumName.toUpperCase().includes(searchParam.toUpperCase()) 
                                ||  
                                albumsContext.albumsData[album].albumArtistsReadable.toUpperCase().includes(searchParam.toUpperCase())
                            ) 
                            && 
                            <Album setShowAlbumModal={setShowAlbumModal} albumMachineName={album} album={albumsContext.albumsData[album]}/> 
                        }
                    </React.Fragment>
                ))}
            </div>
            
        </div>
    );
}

export default Albums;