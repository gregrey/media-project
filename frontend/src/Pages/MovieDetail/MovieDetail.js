import React from 'react';
import './MovieDetail.scss';
import { MoviesContext } from '../../Context/Context';
import IconPlay from '../../Resources/Icons/IconPlay';

const MovieHeader = ({movie}) => {

    const [showMovie, setShowMovie] = React.useState();

    const movieRef = React.useRef();

    const handlePlay = () => {
        setShowMovie(true);
        movieRef.current.play();
    }

    return (
        <div className="movie--header__wrapper">
            <div style={{backgroundImage: `url(http://localhost:1337/get/movie/cover/${movie.movieFolderName}/${movie.movieBanner}`}} className="movie--image--opacity__wrapper">
                <button type="button" onClick={() => {handlePlay()}}><IconPlay /></button>
                <div className="video__wrapper">
                    <video className={`${showMovie && "visible"}`} ref={movieRef} width="75%" id="videoPlayer" controls crossOrigin="anonymous">
                        <source src={`http://localhost:1337/get/video/${movie.movieFolderName}/${movie.movieFolderName}/${movie.movieExtension}`} type="video/mp4" />
                        <track label="English" kind="subtitles" srcLang="en" src={`http://localhost:1337/get/subtitles/${movie.movieFolderName}`} type="text/vtt" default />
                    </video>
                </div>
            </div>
        </div>
    )
}

const MovieDetail = (props) => {
    const movieName = props.match.params.name;    

    const [movie, setMovie] = React.useState();

    React.useEffect(() => { 
        fetch(`http://localhost:1337/get/movies`).then(res => res.json()).then(
            result => { 
                setMovie({...result[movieName]});
            },
            error => { console.log(error) }
        );
    }, [movieName]);

    console.log(movie);

    return (
        <div className="movie--detail__outer-wrapper">
            {movie && 
                <>
                    <MovieHeader movie={movie}/>
                </>
            }
        </div>
    );
}

export default MovieDetail;