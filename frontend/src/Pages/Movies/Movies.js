import React from 'react';
import { Link } from 'react-router-dom'
import './Movies.scss'
import { MoviesContext } from '../../Context/Context';

const Movie = ({movieMachineName, movie}) => {
    return (
        <div  className="movies--item__wrapper">
            <div className="movie--cover__wrapper">
                <Link to={`/movies/movie/${movie.movieFolderName}`}>
                    <img alt="movie--cover" src={`http://localhost:1337/get/movie/cover/${movie.movieFolderName}/${movie.moviePoster}`} />
                </Link>
            </div>
            {/* <div className="movie--title__wrapper">
                <Link to={`/music/movie/${movie.movieFolder}`}>{movie.movieName}</Link>
            </div> */}
        </div>
    )
}

const Movies = () => {
    const moviesContext = React.useContext(MoviesContext);
    console.log(moviesContext.moviesData);
    return (
        <div className="movies__outer-wrapper">
            <div className="movies--list__wrapper">
                <div className="movies--header__wrapper">
                    <h1>Movies</h1>
                </div>
                {Object.keys(moviesContext.moviesData).sort().map((movie, i) => (
                    <React.Fragment key={i}>
                        <Movie movieMachineName={movie} movie={moviesContext.moviesData[movie]}/> 
                    </React.Fragment>
                ))}
            </div>
        </div>
    );
}

export default Movies;