import React from 'react';
import './PageWrapper.scss';
import { Switch, Route } from 'react-router-dom'
import { Scrollbars } from 'react-custom-scrollbars';
import { MusicIsPlayingContext, AudioContext } from '../Context/Context';
import Manager from '../Manager/Manager';
import AlbumSubmit from '../Manager/AlbumSubmit/AlbumSubmit';
import Albums from './Albums/Albums';
import AlbumDetail from './AlbumDetail/AlbumDetail';
import Playlists from './Playlists/Playlists';
import PlaylistSubmit from '../Manager/PlaylistSubmit/PlaylistSubmit';
import PlaylistDetail from './PlaylistDetail/PlaylistDetail';
import MovieSubmit from '../Manager/MovieSubmit/MovieSubmit';
import Movies from './Movies/Movies';
import MovieDetail from './MovieDetail/MovieDetail';

const PageWrapper = () => {
    const audioContext = React.useContext(AudioContext);
    const musicIsPlayingContext = React.useContext(MusicIsPlayingContext);

    const spacebarFunction = (e) => {
        if(e.keyCode === 32) {
            const element = document.activeElement.tagName;
            if(element === 'INPUT') {
                //input is selected so spacebar has to work
            } else {
                e.preventDefault();
                //check playlist context to see if audio is loaded. if not, do nothing. if it is, play or pause depending on if it is playing or not. 
                console.log(audioContext.audioData);
                if(audioContext.audioData.trackNumber) {
                    musicIsPlayingContext.contextSetMusicIsPlaying(!musicIsPlayingContext.musicIsPlaying);
                    console.log("Music is now playing: ", musicIsPlayingContext.musicIsPlaying);
                }
            }
        }
    };

    const test = 0;

    React.useEffect(() => {
        document.addEventListener("keydown", spacebarFunction, false);
        return () => {
          document.removeEventListener("keydown", spacebarFunction, false);
        };
    });

    const scrollRef = React.useRef();

    const scrollFn = () => {
        const offset = scrollRef.current.getScrollTop();
        console.log(offset);
    }

    //ref={scrollRef} onUpdate={() => {console.log('render view')}} onScrollStop={() => {scrollFn()}}
    
    return (
        <main className="page__outer-wrapper">
            <Scrollbars  style={{ }}>
                <div className="page__inner-wrapper">
                    <Switch>
                        <Route path='/manager/add-album' render={(props) => <AlbumSubmit/> }/>
                        <Route path='/manager/add-playlist' render={(props) => <PlaylistSubmit/> }/>
                        <Route path='/manager/add-movie' render={(props) => <MovieSubmit/> }/>
                        {/* <Route path='/manager/add-artist' render={(props) => <ArtistSubmit/> }/>*/}
                        <Route path='/manager' render={(props) => <Manager/>}/> 
                        
                        <Route path="/movies/movie/:name" render={(props) => <MovieDetail {...props}/>}/>
                        <Route path="/music/album/:name" render={(props) => <AlbumDetail {...props}/>}/>
                        <Route path="/music/playlist/:name" render={(props) => <PlaylistDetail {...props} /> }/>
                        {/* <Route path="/music/artist/:name" render={(props) => <ArtistPage {...props}/>}/> */}
                        <Route path='/movies' render={(props) => <Movies /> }/>
                        <Route path='/music' render={(props) => <Albums/>  }></Route>
                        <Route path='/playlists' render={(props) => <Playlists/> }></Route>
                    </Switch>
                </div>    
            </Scrollbars>       
        </main>
    );
}

export default PageWrapper;