import React from 'react';
import './PlaylistCollage.scss';

const PlaylistCollage = ({playlist}) => {
    const tracks = playlist.playlistTrackList;

    const renderImages = () => {
        if(tracks) {
            if(tracks.length < 2) {
                return (
                    <React.Fragment>
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                    </React.Fragment>
                )
            } else if (tracks.length < 3) {
                return (
                    <React.Fragment>
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                        <img alt="album--cover" src={tracks[1].albumImageSource} />
                        <img alt="album--cover" src={tracks[1].albumImageSource} />
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                    </React.Fragment>
                )
            } else if (tracks.length < 4) {
                return (
                    <React.Fragment>
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                        <img alt="album--cover" src={tracks[1].albumImageSource} />
                        <img alt="album--cover" src={tracks[2].albumImageSource} />
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                    </React.Fragment>
                ) 
            } else if (tracks.lenth < 5) {
                return (
                    <React.Fragment>
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                        <img alt="album--cover" src={tracks[1].albumImageSource} />
                        <img alt="album--cover" src={tracks[2].albumImageSource} />
                        <img alt="album--cover" src={tracks[3].albumImageSource} />
                    </React.Fragment>
                )
            } else {
                return (
                    <React.Fragment>
                        <img alt="album--cover" src={tracks[0].albumImageSource} />
                        <img alt="album--cover" src={tracks[1].albumImageSource} />
                        <img alt="album--cover" src={tracks[2].albumImageSource} />
                        <img alt="album--cover" src={tracks[3].albumImageSource} />
                    </React.Fragment>
                )
            }
        } else {
            return null
        }
    }

    return (
        <div className="playlist--collage__outer-wrapper">
            {renderImages()}
        </div>
    );
}

export default PlaylistCollage;