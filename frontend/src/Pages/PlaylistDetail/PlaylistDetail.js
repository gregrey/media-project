import React from 'react';
import IconPlay from '../../Resources/Icons/IconPlay';
import './PlaylistDetail.scss';
import { AudioContext, MusicIsPlayingContext, PlaylistContext, PlaylistsContext } from '../../Context/Context';
import PlaylistCollage from '../PlaylistCollage/PlaylistCollage';

const PlaylistTrack = ({playlist, track, contextWriteAudio, audioData}) => {

    const playlistContext = React.useContext(PlaylistContext);
    const musicIsPlayingContext = React.useContext(MusicIsPlayingContext);

    console.log(playlistContext);

    const handlePlayButton = () => {
        console.log(playlist);
        contextWriteAudio({...track});
        playlistContext.contextWritePlaylist({...playlist});
        musicIsPlayingContext.contextSetMusicIsPlaying(true);
    }

    const [trackIsPlaying, setTrackIsPlaying] = React.useState(false);

    React.useEffect(() => {
        //add active state to track if it is in player
        if(track.trackFileName && audioData.trackFileName) {
            setTrackIsPlaying(track.trackFileName.toString() === audioData.trackFileName.toString());
        }
    }, [audioData.trackFileName, track.trackFileName]);
    
    return (
        <div className={`playlist--track__wrapper ${trackIsPlaying ? `playing` : `disabled`}`}>
            <div className="track--number">
                <button onClick={() => {handlePlayButton()}} className="play-button__wrapper">
                    <IconPlay/>
                </button>
                <span>{track.trackNumber}</span> 
            </div>
            <div className="track--title">
                <span className="title">{track.trackName}</span>
                <span className="artists">&nbsp;-&nbsp;{track.trackArtistsReadable}{track.trackFeatArtistsReadable.length > 0 && `, ${track.trackFeatArtistsReadable}`}</span>
            </div>
        </div>
    )
}

const PlaylistTracks = ({playlist}) => {
    const audioContext = React.useContext(AudioContext);
    return (
        <>
            {playlist && 
                <div className="playlist--tracks__wrapper">
                    <div className="playlist--track__wrapper">
                        <div className="track--number">{` #`}</div>
                        <div className="track--title"><span className="title">Track name</span></div>
                    </div>
                    {
                        playlist.playlistTrackList.map((track, i) => {
                            return (
                                <PlaylistTrack contextWriteAudio={audioContext.contextWriteAudio} audioData={audioContext.audioData} playlist={playlist} track={track} key={i}/>
                            )
                        })
                    }
                </div>
            }
        </>
    )
}

const PlaylistHeader = ({playlist}) => {
    const playlistContext = React.useContext(PlaylistContext);
    const audioContext = React.useContext(AudioContext);
    const musicIsPlayingContext = React.useContext(MusicIsPlayingContext);

    const handlePlayPlaylist = () => {
        const track = playlist.playlistTrackList[0];
        audioContext.contextWriteAudio({...track});
        playlistContext.contextWritePlaylist({...playlist});
        musicIsPlayingContext.contextSetMusicIsPlaying(true);
    }

    return (
        <div className="playlist--header__wrapper">
            <div className="playlist--image__wrapper">
                <PlaylistCollage playlist={playlist}/>
            </div>
            <div className="playlist--text__wrapper">
                <h3>Playlist</h3>
                <div className="playlist--title__wrapper">
                    <h1>{playlist.playlistName}</h1>
                </div>
                <div className="playlist--play__wrapper">
                    <div className="form--button__wrapper">
                        <button onClick={() => {handlePlayPlaylist()}} type="button">Play playlist</button>
                    </div>
                </div> 
            </div>
        </div>
    )
}

const PlaylistDetail = (props) => {
    const playlistName = props.match.params.name;
    const playlistsContext = React.useContext(PlaylistsContext);

    const playlist = playlistsContext.playlistsData[playlistName];

    return (
        <div className="playlist--detail__outer-wrapper">
            {playlist && 
                <>
                    <PlaylistHeader playlist={playlist}/>
                    <PlaylistTracks playlist={playlist}/>
                </>
            }
        </div>
    );
}

export default PlaylistDetail;