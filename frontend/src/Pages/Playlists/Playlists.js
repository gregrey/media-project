import React from 'react';
import { Link } from 'react-router-dom'
import './Playlists.scss'
import { PlaylistsContext } from '../../Context/Context';
import PlaylistCollage from '../PlaylistCollage/PlaylistCollage';

const Playlist = ({playlistMachineName}) => {
    const playlistsContext = React.useContext(PlaylistsContext);
    const playlist = playlistsContext.playlistsData[playlistMachineName];

    if (playlist.playlistTrackList.length > 0) {
        return (
            <div className="playlists--item__wrapper">
                <div className="playlist--cover__wrapper">
                    <PlaylistCollage playlist={playlist}/>
                </div>
                <div className="playlist--title__wrapper">
                    <Link to={`/music/playlist/${playlist.playlistMachineName}`}>{playlist.playlistName}</Link> 
                </div>
            </div>
        )
    } else {
        return null
    }

    
}

const Playlists = () => {
    const playlistsContext = React.useContext(PlaylistsContext);
    return (
        <div className="playlists__outer-wrapper">
            <div className="playlists--list__wrapper">
                <div className="playlists--header__wrapper">
                    <h1>Playlists</h1>
                </div>
                {Object.keys(playlistsContext.playlistsData).sort().map((playlist, i) => (
                    <React.Fragment key={i}>
                        {<Playlist playlistMachineName={playlist}/>}
                    </React.Fragment>
                ))}
            </div>
        </div>
    );
}

export default Playlists;