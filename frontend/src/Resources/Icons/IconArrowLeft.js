import React from 'react';

const IconArrowLeft = () => {
    return (
        <div className="icon__wrapper icon__arrow--left">
            <svg id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlnsXlink='http://www.w3.org/1999/xlink' viewBox='0 0 18 14'>
                <defs>
                    <rect id='SVGID_1_' width='18' height='14' />
                </defs>
                <clipPath id='SVGID_2_'>
                    <use xlinkHref='#SVGID_1_' overflow='visible' />
                </clipPath>
                <g id='arrow-left' className='st0'>
                    <path id='Combined_Shape-2' d='M6.3,13.7l-6-6l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0v0l0,0v0 l0,0l0,0v0l0,0l0,0l0,0l0,0C0,7.1,0,6.9,0.1,6.6l0,0l0,0v0l0,0l0,0v0c0-0.1,0.1-0.2,0.2-0.3l6-6c0.4-0.4,1-0.4,1.4,0 c0.4,0.4,0.4,1,0,1.4L3.4,6H17c0.6,0,1,0.4,1,1s-0.4,1-1,1H3.4l4.3,4.3c0.4,0.4,0.4,1,0,1.4C7.3,14.1,6.7,14.1,6.3,13.7 C6.3,13.7,6.3,13.7,6.3,13.7z'
                    />
                    <defs>
                        <path id='SVGID_3_' d='M6.3,13.7l-6-6l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0v0l0,0v0l0,0 l0,0v0l0,0l0,0l0,0l0,0C0,7.1,0,6.9,0.1,6.6l0,0l0,0v0l0,0l0,0v0c0-0.1,0.1-0.2,0.2-0.3l6-6c0.4-0.4,1-0.4,1.4,0 c0.4,0.4,0.4,1,0,1.4L3.4,6H17c0.6,0,1,0.4,1,1s-0.4,1-1,1H3.4l4.3,4.3c0.4,0.4,0.4,1,0,1.4C7.3,14.1,6.7,14.1,6.3,13.7 C6.3,13.7,6.3,13.7,6.3,13.7z'
                        />
                    </defs>
                    <clipPath id='SVGID_4_'>
                        <use xlinkHref='#SVGID_3_' overflow='visible' />
                    </clipPath>
                </g>
            </svg>
        </div>
    );
}

export default IconArrowLeft;