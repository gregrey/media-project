import React from 'react';

const IconDisc = () => {
    return (
        <div className="icon__wrapper icon__disc">
            <svg
                id="prefix__Layer_1"
                x={0}
                y={0}
                viewBox="0 0 22 22"
                xmlSpace="preserve"
            >
                <style>
                    {
                        '.prefix__st0{stroke:#000;stroke-width:2;stroke-linecap:round;stroke-linejoin:round}'
                    }
                </style>
                <circle className="prefix__st0" cx={11} cy={11} r={10} />
                <path
                    className="prefix__st0"
                    d="M1 11h20M11 1c2.5 2.7 3.9 6.3 4 10-.1 3.7-1.5 7.3-4 10-2.5-2.7-3.9-6.3-4-10 .1-3.7 1.5-7.3 4-10z"
                />
            </svg>
        </div>
    );
}

export default IconDisc;