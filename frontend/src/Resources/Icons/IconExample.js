import React from 'react';

const IconExample = () => {
    return (
        <div className="icon__wrapper icon__name">
            <svg></svg>
        </div>
    );
}

export default IconExample;