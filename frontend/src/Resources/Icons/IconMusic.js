import React from 'react';

const IconMusic = () => {
    return (
        <div className="icon__wrapper icon__music">
            <svg viewBox="0 0 22 22">
                <path
                d="M0 4v18h22V4H0zm1 1v16h20V5H1zm1-3v1h18V2H2zm2-2v1h14V0H4zm5.2 11.2l5.3-1.3v4.8c-.3-.1-.6-.2-.9-.2-.2 0-.6.1-.8.2-.5.3-.8.7-.8 1.1 0 .2.1.5.2.6.2.2.5.3.9.3.5 0 .9-.2 1.3-.5s.5-.8.5-1.3V8.1L8.7 9.6v6.6c-.2-.1-.5-.2-.8-.2-.2 0-.4 0-.7.1-.6.2-1 .7-1 1.2 0 .2.1.5.2.6.2.2.5.3.9.3.6 0 1.4-.5 1.7-1 .1-.2.2-.6.2-.8 0 0 0 .3 0 0v-5.2z"
                fillRule="evenodd"
                clipRule="evenodd"
                />
            </svg>
        </div>
    );
}

export default IconMusic;