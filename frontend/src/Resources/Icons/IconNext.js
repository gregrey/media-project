import React from "react";

const IconNext = () => {
    return (
        <div className="icon__wrapper icon__next">
            <svg
                id="prefix__Layer_1"
                x={0}
                y={0}
                viewBox="0 0 16 18"
                xmlSpace="preserve"
            >
                <style>
                    {
                        ".prefix__st0{fill:none;stroke:#000;stroke-width:2;stroke-linecap:round;stroke-linejoin:round}"
                    }
                </style>
                <path className="prefix__st0" d="M1 1l10 8-10 8zM15 2v14" />
            </svg>
        </div>
    );
};

export default IconNext;
