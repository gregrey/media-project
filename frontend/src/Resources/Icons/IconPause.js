import React from 'react';

const IconPause = () => {
    return (
        <div className="icon__wrapper icon__pause">
        <svg
            id="prefix__Layer_1"
            x={0}
            y={0}
            viewBox="0 0 14 18"
            xmlSpace="preserve">
            <style>
            {
                '.prefix__st0{fill:none;stroke:#000;stroke-width:2;stroke-linecap:round;stroke-linejoin:round}'
            }
            </style>
            <path className="prefix__st0" d="M1 1h4v16H1zM9 1h4v16H9z" />
        </svg>
        </div>
    );
}

export default IconPause;