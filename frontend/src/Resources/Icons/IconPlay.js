import React from 'react';

const IconPlay = () => {
    return (
        <div className="icon__wrapper icon__play">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 20"><path fill="none" stroke="#000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" d="M1 1l14 9-14 9z"/></svg>
        </div>
    );
}

export default IconPlay;