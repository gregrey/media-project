import React from 'react';

const IconPlus = () => {
    return (
        <div className="icon__wrapper icon__plus">
            <svg id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlnsXlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'>
                <defs>
                    <rect id='SVGID_1_' width='16' height='16' />
                </defs>
                <clipPath id='SVGID_2_'>
                    <use xlinkHref='#SVGID_1_' overflow='visible' />
                </clipPath>
                <g id='plus' className='st0'>
                    <path id='Combined_Shape-2' d='M7,15V9H1C0.4,9,0,8.6,0,8s0.4-1,1-1h6V1c0-0.6,0.4-1,1-1s1,0.4,1,1v6h6c0.6,0,1,0.4,1,1 s-0.4,1-1,1H9v6c0,0.6-0.4,1-1,1S7,15.6,7,15z'
                    />
                    <defs>
                        <path id='SVGID_3_' d='M7,15V9H1C0.4,9,0,8.6,0,8s0.4-1,1-1h6V1c0-0.6,0.4-1,1-1s1,0.4,1,1v6h6c0.6,0,1,0.4,1,1s-0.4,1-1,1H9v6 c0,0.6-0.4,1-1,1S7,15.6,7,15z'
                        />
                    </defs>
                    <clipPath id='SVGID_4_'>
                        <use xlinkHref='#SVGID_3_' overflow='visible' />
                    </clipPath>
                </g>
            </svg>
        </div>
    );
}

export default IconPlus;