import React from 'react';

const IconPrevious = () => {
    return (
        <div className="icon__wrapper icon__previous">
            <svg id="prefix__Layer_1"
                x={0}
                y={0}
                viewBox="0 0 16 18"
                xmlSpace="preserve">
                <style>
                    {
                        '.prefix__st0{fill:none;stroke:#000;stroke-width:2;stroke-linecap:round;stroke-linejoin:round}'
                    }
                </style>
                <path className="prefix__st0" d="M15 17L5 9l10-8zM1 16V2" />
            </svg>
        </div>
    );
}

export default IconPrevious;