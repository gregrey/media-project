import React from 'react';

const IconRepeat = () => {
    return (
        <div className="icon__wrapper icon__repeat">
            <svg id="prefix__Layer_1" x={0} y={0} viewBox="0 0 20 24" xmlSpace="preserve">
                <style>{'.prefix__st0{fill:none;stroke:#000;stroke-width:2;stroke-linecap:round;stroke-linejoin:round}'}</style>
                <path className="prefix__st0" d="M15 1l4 4-4 4" />
                <path className="prefix__st0" d="M1 11V9c0-2.2 1.8-4 4-4h14M5 23l-4-4 4-4"/>
                <path className="prefix__st0" d="M19 13v2c0 2.2-1.8 4-4 4H1" />
            </svg>
        </div>
    );
}

export default IconRepeat;