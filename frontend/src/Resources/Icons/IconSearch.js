import React from 'react';

const IconSearch = () => {
    return (
        <div className="icon__wrapper icon__search">
            <svg id='Layer_1' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 17 17'>
                <path id='search' className='st0' d='M16.4,17c-0.2,0-0.3-0.1-0.4-0.2l-5.1-5.1c-1.2,1-2.7,1.5-4.2,1.5c-3.7,0-6.7-2.9-6.7-6.6	S2.9,0,6.6-0.1s6.7,2.9,6.7,6.6c0,1.6-0.5,3.1-1.5,4.3l5.1,5.1c0.2,0.2,0.2,0.6,0,0.8c0,0,0,0,0,0C16.7,16.9,16.6,17,16.4,17z M6.6,1.1c-3,0-5.5,2.5-5.5,5.5s2.5,5.5,5.5,5.5s5.5-2.5,5.5-5.5C12.1,3.6,9.6,1.1,6.6,1.1z'/>
            </svg>
        </div>
    );
}

export default IconSearch;  