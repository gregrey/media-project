import React from 'react';

const IconShuffle = () => {
    return (
        <div className="icon__wrapper icon__shuffle">
            <svg
                id="prefix__Layer_1"
                x={0}
                y={0}
                viewBox="0 0 19 20"
                xmlSpace="preserve"
            >
                <style>
                    {
                        '.prefix__st0{fill:none;stroke:#000;stroke-width:2;stroke-linecap:round;stroke-linejoin:round}'
                    }
                </style>
                <path
                    className="prefix__st0"
                    d="M13 1h5v5M1 18L18 1M18 14v5h-5M12 13l6 6M1 2l5 5"
                />
            </svg>
        </div>
    );
}

export default IconShuffle;