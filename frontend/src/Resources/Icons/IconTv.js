import React from 'react';

const IconTv = () => {
    return (
        <div className="icon__wrapper icon__tv">
            <svg viewBox="0 0 312.9 254">
                <path
                d="M0 0h312.9v198.3h-127c0 5.4.6 9.6-3.4 13.9l42.4 29.5c7.2 5-.4 15.9-7.6 10.9l-52-36.2c-6.9 0-21.6.9-27.6-.2l-52.3 36.4c-7.2 5-14.8-5.9-7.6-10.9l49.8-34.6c-.8-2.7-.5-6-.5-8.8H0V0zm299.7 185.1V13.2H13.2v171.9h286.5z"
                fillRule="evenodd"
                clipRule="evenodd"
                />
            </svg>
        </div>
    );
}

export default IconTv;