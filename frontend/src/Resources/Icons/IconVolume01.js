import React from 'react';

const IconVolume01 = () => {
    return (
        <div className="icon__wrapper volume--01">
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 69 57.4"
            version="1.1"
            viewBox="0 0 69 57.4"
            xmlSpace="preserve"
            >
            <path d="M38.1 57.4c-.3 0-.6-.1-.9-.3l-22-16.6H2.9c-1.6 0-2.9-1.3-2.9-2.9V19.8c0-1.6 1.3-2.9 2.9-2.9h12.4L37.3.3c.4-.3 1-.4 1.5-.1.5.2.8.8.8 1.3V56c0 .6-.3 1.1-.8 1.3-.2.1-.4.1-.7.1z"></path>
            </svg>
        </div>
    );
}

export default IconVolume01;