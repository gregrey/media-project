import React from 'react';

const IconVolume02 = () => {
    return (
        <div className="icon__wrapper volume--02">
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 69 57.4"
            version="1.1"
            viewBox="0 0 69 57.4"
            xmlSpace="preserve"
            >
            <path d="M38.1 57.4c-.3 0-.6-.1-.9-.3l-22-16.6H2.9c-1.6 0-2.9-1.3-2.9-2.9V19.8c0-1.6 1.3-2.9 2.9-2.9h12.4L37.3.3c.4-.3 1-.4 1.5-.1.5.2.8.8.8 1.3V56c0 .6-.3 1.1-.8 1.3-.2.1-.4.1-.7.1zM43.7 41.1c-.4 0-.8-.1-1-.4-.6-.6-.6-1.5 0-2.1 2.7-2.7 4.1-6.2 4.1-9.9 0-3.8-1.5-7.3-4.1-9.9-.6-.6-.6-1.5 0-2.1.6-.6 1.5-.6 2.1 0 3.2 3.2 5 7.5 5 12s-1.8 8.8-5 12c-.3.3-.7.4-1.1.4z"></path>
            </svg>
        </div>
    );
}

export default IconVolume02;