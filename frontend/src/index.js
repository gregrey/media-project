import React from 'react';
import ReactDOM from 'react-dom';
import AppWrapper from './AppWrapper/AppWrapper';
import './Theme/Index.scss';

ReactDOM.render(<AppWrapper />, document.getElementById('root'));
