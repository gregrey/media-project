const express = require('express');
const router = express.Router();
const editJsonFile = require("edit-json-file");
const path = require("path");

router.get('/:file', (req, res) => {
    const file = req.params.file;
    const database_album = editJsonFile(path.join(__dirname, `../../cloud/music/${file}`, `${file}.json`));
    res.send(database_album.toObject())
})

module.exports = router;