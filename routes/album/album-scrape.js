const request = require('request');
const cheerio = require('cheerio');
const express = require('express');
const router = express.Router();


router.get('/:album/:release/:id', (req, res) => {
    const album = req.params.album;
    const release = req.params.release;
    const id = req.params.id;
    const url = `https://www.discogs.com/${album}/${release}/${id}`;

    const trackList = [];
    const albumInfo = {};

    request(url, (error, response, html) => {
        if(!error && response.statusCode == 200) {
            let $ = cheerio.load(html);
            albumInfo.albumArtists = (($('#profile_title a').text()).trim()).replace(/ *\([^)]*\) */g, "");
            albumInfo.albumName = ($('#profile_title > span:last-of-type').text()).trim();
            albumInfo.albumTrackAmount = ($('.playlist tr').length).toString();
            albumInfo.albumDiscogs = url;
            albumInfo.albumLabel = (($('.profile .content:nth-of-type(2) a').text())).replace(/ *\([^)]*\) */g, "").trim();

            $('.playlist tr.track').each(function(i, tr) {
                const track = cheerio.load(tr);
                const newTrack = {};
                let trackArtists = "";

                if(track('.tracklist_track_artists').length > 0) {
                    track('.tracklist_track_artists a').each(function(i, a){
                        let link = cheerio.load(a);
                        let artist = ((link.text()).replace(/ *\([^)]*\) */g, "")).trim();
                        if(i > 0) {
                            trackArtists += `, ${artist}`;
                        } else {
                            trackArtists += artist;
                        }
                    });

                } else {
                    trackArtists = albumInfo.albumArtists;
                }

                let trackName = (track('span.tracklist_track_title').text()).trim().replace("rmx", "Remix").replace("RMX", "Remix").replace("Rmx", "Remix");
                let trackRemixArtists;

                if(trackName.includes("Remix")){
                    trackRemixArtists = "-------------- FILL IN REMIX ARTIST --------------";
                } else {
                    trackRemixArtists = "";
                }

                newTrack.trackName = trackName;
                newTrack.trackArtists = trackArtists;
                newTrack.trackRemixArtists = trackRemixArtists;
                newTrack.trackFeatArtists = "";

                trackList.push(newTrack);

            });
            const result = {
                albumInfo: {...albumInfo},
                albumTracks: [...trackList]
            }
            res.send(result)

        } else {
            console.log(error);
        }

    });
    
})

module.exports = router;