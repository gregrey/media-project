const express = require('express');
const router = express.Router();
const editJsonFile = require("edit-json-file");
const path = require("path");
const utility = require("../../utility/utility")

router.get('/', (req, res) => {
    let database_albums = editJsonFile(path.join(__dirname, '../../database/', 'albums.json'));
    res.send(database_albums.toObject())
})

module.exports = router;