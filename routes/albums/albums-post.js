const express = require('express');
const router = express.Router();
const editJsonFile = require("edit-json-file");
const path = require("path");
const utility = require("../../utility/utility")

router.post('/', (req, res) => {
    //save request body to variable
    const album = req.body;

    console.log(album);

    //initialise JSON file, create if it does not exist
    const database_albums = editJsonFile(path.join(__dirname, '../../database/', 'albums.json'), { autosave: true });

    //artist_albumname_unix
    //const key = `${utility.convertToMachine(album.albumArtists)}_${utility.convertToMachine(album.albumName)}_${album.albumRelease}`;
    const key = `${album.albumFolder}`;

    const database_album = editJsonFile(path.join(__dirname, `../../cloud/music/${key}`, `${key}.json`), { autosave: true });

    //initialise tracks array
    let returnArray = [];

    for(let i = 0; i < album.albumTracks.length; i++) {
        const track = album.albumTracks[i];
        const trackArtists = track.trackArtists.length > 0 ? utility.returnArtistsArray(track.trackArtists) : [];
        const trackRemixArtists = track.trackRemixArtists.length > 0 ? utility.returnArtistsArray(track.trackRemixArtists) : [];
        const trackFeatArtists = track.trackFeatArtists.length > 0 ?  utility.returnArtistsArray(track.trackFeatArtists) : [];

        returnArray.push({
            trackNumber: utility.extendInt(i + 1),
            trackId: i,
            trackFileName: `${utility.extendInt(i + 1)}_${key}.mp3`,
            trackName: track.trackName,
            trackArtists: trackArtists,
            trackArtistsReadable: track.trackArtists,
            trackRemixArtists: trackRemixArtists,
            trackRemixArtistsReadable: track.trackRemixArtists,
            trackFeatArtists: trackFeatArtists,
            trackFeatArtistsReadable: track.trackFeatArtists,
            trackNextTrackSource: i + 1 < album.albumTracks.length ? `http://localhost:1337/get/audio/${key}/${utility.extendInt(i+2)}_${key}.mp3` : false,
            trackNextTrackAlbum: i + 1 < album.albumTracks.length ? key : false,
            trackNextTrackId: i + 1 < album.albumTracks.length ? i + 1 : false,
            trackNextTrackName: i + 1 < album.albumTracks.length ? `${utility.extendInt(i+2)}_${key}.mp3` : false,
            trackPreviousTrackSource: i > 0 ? `http://localhost:1337/get/audio/${key}/${utility.extendInt(i)}_${key}.mp3` : false,
            trackPreviousAlbum: i > 0 ? key : false,
            trackPreviousId: i > 0 ? i - 1 : false,
            trackPreviousName: i > 0 ? `${utility.extendInt(i)}_${key}.mp3` : false,
            trackSource: `http://localhost:1337/get/audio/${key}/${utility.extendInt(i+1)}_${key}.mp3`,
            albumUrl: `/music/album/${key}`,
            albumTrackAmount: album.albumTrackAmount,
            albumName: album.albumName,
            albumMachineName: utility.convertToMachine(album.albumName),
            albumFolder: key,
            albumImage: `${key}.jpg`,
            albumImagePath: `.cloud/music/${key}/${key}.jpg`,
            albumImageSource: `http://localhost:1337/get/album/cover/${key}/${key}.jpg`,
            albumUrl: `/music/album/${key}`,
        })

    }

    //write album to JSON file
    database_albums.set(`${key}`, {
        albumArtists: utility.returnArtistsArray(album.albumArtists),
        albumArtistsReadable: album.albumArtists,
        albumDateAddedUnix: `${utility.generateTimestamp()}`,
        albumDateAddedHuman: `${utility.generateReadableTimestamp(utility.generateTimestamp())}`,
        albumDiscogs: album.albumDiscogs,
        albumFolder: key,
        albumGenres: utility.returnGenresArray(album.albumGenres),
        albumImage: `${key}.jpg`,
        albumImagePath: `.cloud/music/${key}/${key}.jpg`,
        albumImageSource: `http://localhost:1337/get/album/cover/${key}/${key}.jpg`,
        albumLabel: album.albumLabel,
        albumMachineLabel: utility.convertToMachine(album.albumLabel),
        albumMachineName: utility.convertToMachine(album.albumName),
        albumName: album.albumName,
        albumTrackAmount: album.albumTrackAmount,
        albumUrl: `/music/album/${key}`,
        //albumTrackList: [...returnArray],
        albumRelease: album.albumRelease
    });

    //write album to album folder JSON file
    database_album.set(`${key}`, {
        albumArtists: utility.returnArtistsArray(album.albumArtists),
        albumArtistsReadable: album.albumArtists,
        albumDateAddedUnix: `${utility.generateTimestamp()}`,
        albumDateAddedHuman: `${utility.generateReadableTimestamp(utility.generateTimestamp())}`,
        albumDiscogs: album.albumDiscogs,
        albumFolder: key,
        albumGenres: utility.returnGenresArray(album.albumGenres),
        albumImage: `${key}.jpg`,
        albumImagePath: `.cloud/music/${key}/${key}.jpg`,
        albumImageSource: `http://localhost:1337/get/album/cover/${key}/${key}.jpg`,
        albumLabel: album.albumLabel,
        albumMachineLabel: utility.convertToMachine(album.albumLabel),
        albumMachineName: utility.convertToMachine(album.albumName),
        albumName: album.albumName,
        albumTrackAmount: album.albumTrackAmount,
        albumUrl: `/music/album/${key}`,
        albumTrackList: [...returnArray],
        albumRelease: album.albumRelease
    });

    res.sendStatus(200);

})

module.exports = router;
