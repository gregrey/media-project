const express = require('express');
const router = express.Router();
var fs = require('fs');

router.get('/:folder/:image', function(req, res) {
    folder = req.params.folder;
    image = req.params.image;
    fs.readFile(`./cloud/music/${folder}/${image}`, function(err, data){
        if(err){
            res.statusCode = 500;
            res.end(`Error getting the file: ${err}.`);
        } else {
            res.setHeader('Content-type', 'image/jpeg' );
            res.end(data);
        }
    });
})



module.exports = router;