const express = require('express');
const router = express.Router();
const editJsonFile = require("edit-json-file");
const path = require("path");
const utility = require("../../utility/utility")

router.get('/', (req, res) => {
    let database_movies = editJsonFile(path.join(__dirname, '../../database/', 'movies.json'));
    console.log('getting movies')
    res.send(database_movies.toObject())
})

module.exports = router;