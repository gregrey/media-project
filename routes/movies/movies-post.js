const express = require('express');
const router = express.Router();
const editJsonFile = require("edit-json-file");
const path = require("path");
const utility = require("../../utility/utility")

router.post('/', (req, res) => {
    //save request body to variable
    const movie = req.body;
    
    //initialise JSON file, create if it does not exist
    const database_movies = editJsonFile(path.join(__dirname, '../../database/', 'movies.json'), { autosave: true });

    //artist_albumname_unix
    //const key = `${utility.convertToMachine(album.albumArtists)}_${utility.convertToMachine(album.albumName)}_${album.albumRelease}`;
    const key = `${movie.movieFolderName}`;

    //write album to JSON file
    database_movies.set(`${key}`, {
        movieName: movie.movieName,
        movieBanner: movie.movieBanner,
        moviePoster: movie.moviePoster,
        movieRelease: movie.movieRelease,
        movieExtension: movie.movieExtension,
        movieFolderName: movie.movieFolderName

    });

    res.sendStatus(200);

})

module.exports = router;