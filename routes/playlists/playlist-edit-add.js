const express = require('express');
const router = express.Router();
const editJsonFile = require("edit-json-file");
const path = require("path");
const utility = require("../../utility/utility")

router.post('/', (req, res) => {
    //save request body to variable
    const playlist = req.body;

    //initialise JSON file, create if it does not exist
    const database_playlists = editJsonFile(path.join(__dirname, '../../database/', 'playlists.json'), { autosave: true });

    const key = `${playlist.playlistMachineName}`;
    const track = playlist.playlistTrack;

    //previous playlist
    const previousPlaylist = database_playlists.read();
    const previousTrackList = previousPlaylist[key].playlistTrackList;

    const newPlaylist = {
        ...previousPlaylist[key],
        playlistTrackList: [
            ...previousTrackList,
            track
        ]
    }
    
    const playlistLength = newPlaylist.playlistTrackList.length;

    if(playlistLength > 1) {
        newPlaylist.playlistTrackList[0].trackNextTrackId = 1;
        newPlaylist.playlistTrackList[0].trackNextTrackSource = true;
    } else {
        newPlaylist.playlistTrackList[0].trackPreviousTrackSource = false;
        newPlaylist.playlistTrackList[0].trackPreviousId = false;
    }

    newPlaylist.playlistTrackList.map( (track, i) => {
        newPlaylist.playlistTrackList[i].trackNumber = ('0'+ (i + 1)).slice(-2);
        newPlaylist.playlistTrackList[i].trackId = i;

        if(i > 0) {
            newPlaylist.playlistTrackList[i].trackPreviousId = i - 1;
            newPlaylist.playlistTrackList[i].trackPreviousTrackSource = true;
            newPlaylist.playlistTrackList[i].trackNextTrackId = i + 1;
            newPlaylist.playlistTrackList[i].trackNextTrackSource = true;
        }

        if(i + 1 === playlistLength) {
            //last track in playlist
            newPlaylist.playlistTrackList[i].trackNextTrackId = false;
            newPlaylist.playlistTrackList[i].trackNextTrackSource = false;
        }

    });

    //write playlist to new playlist
    database_playlists.set(`${key}`, {
        ...newPlaylist,
    });

    res.sendStatus(200);

})

module.exports = router;