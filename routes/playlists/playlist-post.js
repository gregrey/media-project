const express = require('express');
const router = express.Router();
const editJsonFile = require("edit-json-file");
const path = require("path");
const utility = require("../../utility/utility")

router.post('/', (req, res) => {
    //save request body to variable
    const playlist = req.body;
    
    //initialise JSON file, create if it does not exist
    const database_playlists = editJsonFile(path.join(__dirname, '../../database/', 'playlists.json'), { autosave: true });

    //artist_albumname_unix
    //const key = `${utility.convertToMachine(album.albumArtists)}_${utility.convertToMachine(album.albumName)}_${album.albumRelease}`;
    const key = `${playlist.playlistMachineName}`;

    //write album to JSON file
    database_playlists.set(`${key}`, {
        playlistName: playlist.playlistName,
        playlistMachineName: playlist.playlistMachineName,
        playlistTrackList: []
    });

    res.sendStatus(200);

})

module.exports = router;