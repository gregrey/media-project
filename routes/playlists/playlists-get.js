const express = require('express');
const router = express.Router();
const editJsonFile = require("edit-json-file");
const path = require("path");
const utility = require("../../utility/utility")

router.get('/', (req, res) => {
    let database_playlists = editJsonFile(path.join(__dirname, '../../database/', 'playlists.json'));
    res.send(database_playlists.toObject())
})

module.exports = router;