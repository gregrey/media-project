const express = require('express');
const router = express.Router();
var fs = require('fs');

router.get('/:folder', function(req, res) {
    const folder = req.params.folder;
    console.log('getting sub titles');
    fs.readFile(`./cloud/movies/${folder}/${folder}.vtt`, function(err, data){
        if(err){
            res.statusCode = 500;
            res.end(`Error getting the file: ${err}.`);
        } else {
            res.setHeader('Content-type', 'text/vtt' );
            res.end(data);
        }
    });     
})

module.exports = router;