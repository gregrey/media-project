const express = require('express');
const router = express.Router();
var fs = require('fs');

router.get('/:folder/:track/:extension', function(req, res) {
    const folder = req.params.folder;
    const track = req.params.track;
    const extension = req.params.extension;
    const path = `./cloud/movies/${folder}/${track}.${extension}`;
    const stat = fs.statSync(path);
    const fileSize = stat.size;
    const range = req.headers.range;
    console.log(range);
    const parts = range.replace(/bytes=/, "").split("-")
    const start = parseInt(parts[0], 10)
    const end = parts[1] 
        ? parseInt(parts[1], 10)
        : fileSize-1
    const chunksize = (end-start)+1
    const file = fs.createReadStream(path, {start, end})
    const head = {
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/mov',
    }
    res.writeHead(206, head);
    file.pipe(res);       
})

module.exports = router;