const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.json());

//init album routes
const albumsPost = require('./routes/albums/albums-post');
const albumsGet = require('./routes/albums/albums-get');
const albumGet = require('./routes/album/album-get');
app.use('/post/album', albumsPost);
app.use('/get/albums', albumsGet);
app.use('/get/album', albumGet);

//init playlists routes
const playlistsGet = require('./routes/playlists/playlists-get');
const playlistPost = require('./routes/playlists/playlist-post');
const playlistEditAdd = require('./routes/playlists/playlist-edit-add');
app.use('/get/playlists', playlistsGet);
app.use('/post/playlist', playlistPost);
app.use('/post/edit/playlist/add', playlistEditAdd);

//init movies routes
//init playlists routes
const moviesGet = require('./routes/movies/movies-get');
const moviesPost = require('./routes/movies/movies-post');
app.use('/get/movies', moviesGet);
app.use('/post/movies', moviesPost);

//init audio route
const audioGet = require('./routes/audio/audio-get')
app.use('/get/audio', audioGet);

//init video route
const videoGet = require('./routes/video/video-get')
app.use('/get/video', videoGet);

//init subtitles route
const subtitlesGet = require('./routes/video/subtitles-get');
app.use('/get/subtitles', subtitlesGet);

//init image route
const albumCoverGet = require('./routes/image/album-cover-get');
app.use('/get/album/cover', albumCoverGet);

const movieCoverGet = require('./routes/image/movie-cover-get');
app.use('/get/movie/cover', movieCoverGet);

//scrape album
const albumScrape = require('./routes/album/album-scrape');
app.use('/scrape/album', albumScrape);

app.listen(1337, () => {
    console.log('Listening on port 1337');
})