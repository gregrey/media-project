const moment = require("moment");
//1 out of 177 tracks converted with major problems.

//Source: "C:\Development\catmansion-media-dev\cloud\renamed\xerox_illumination_rmx_1248825600\06_xerox_illumination_rmx_1248825600.flac"
//Corrupted FLAC stream
module.exports = {

    //convert a string to a machine name
    convertToMachine: function(string) {
        return (string.toLowerCase()).replace(/ /g,"_").replace(/,/g, '');
    },

    //generate a current timestamp
    generateTimestamp: function() {
        return moment().unix();
    },

    //convert unix timestamp to MMMM DD, YYYYY
    generateReadableTimestamp: function(timestamp) {
        return moment.unix(timestamp).format("MMMM DD, YYYY");
    },

    //turn string into artist array, works for artists, featuring artists, remix artists
    returnArtistsArray: function(string) {
        let artistArray = string.split(", ");
        let returnArray = [];
        for(let i = 0; i < artistArray.length; i++) {
            returnArray.push(
                {
                    "artistName": artistArray[i],
                    "artistMachineName": this.convertToMachine(artistArray[i]),
                    "artistUrl": `/music/artist/${this.convertToMachine(artistArray[i])}`
                }
            )
        }
        return returnArray
    },

    //turn string into genre array
    returnGenresArray: function(string) {
        let genreArray = string.split(", ");
        let returnArray = [];
        for(let i = 0; i < genreArray.length; i++) {
            returnArray.push(
                {
                    "genreName": genreArray[i],
                    "genreMachineName": this.convertToMachine(genreArray[i]),
                    "genreUrl": `/music/genre/${this.convertToMachine(genreArray[i])}`
                }
            )
        }
        return returnArray
    },

    //sort object alphabetically
    sortObject: function(obj) {
        keysSorted = Object.keys(obj).sort(function(a,b){return obj[a]-obj[b]})
    },

    //make number 2 digits long, add 0 before single digits
    extendInt: function(number) {
        return ("0" + (parseInt(number))).slice(-2);
    }

}